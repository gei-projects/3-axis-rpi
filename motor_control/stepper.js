/**
 * Stepper motor class
 *
 * @version 1.0
 * @author S. DI MERCURIO
 *
 */

const io = require('pigpio');
const nanoTimer = require('nanotimer');

const SLOW_DECAY = 1;
const FAST_DECAY = 2;
const HALF_STEP = 10;
const HALF_FULL = 20;
const DIR_CLOCKWISE = 100;
const DIR_COUNTER_CLOCKWISE = 200;
const io = require('pigpio');

class stepper {
  /**
   * Creates an instance of stepper.
   * @constructor
   * @param {number} Control - Control pin number, used for selecting between slow or fast decay
   * @param {number} HalfFull - pin defining if half step or full step are to bue used
   * @param {number} Clock - pin for clocking the stepper
   * @param {number} CwCcw - pin for selecting direction
   * @memberof stepper
   */
  constructor(Control, HalfFull, Clock, CwCcw) {
    this.Control = new io.Gpio(Control, { mode: io.Gpio.OUTPUT });
    this.HalfFull = new io.Gpio(HalfFull, { mode: io.Gpio.OUTPUT });
    this.Clock = new io.Gpio(Clock, { mode: io.Gpio.OUTPUT });
    this.CwCcw = new io.Gpio(CwCcw, { mode: io.Gpio.OUTPUT });

    this.steps = 0;
    this.reset();
  }

  /**
   * Reset all control pins of a stepper to 0, zero all attributes
   * @memberof stepper
   */
  reset() {
    this.Control.digitalWrite(0);
    this.HalfFull.digitalWrite(0);
    this.Clock.digitalWrite(0);
    this.CwCcw.digitalWrite(0);

    this.steps = 0;
  }

  /**
   * Set controller running mode. Choose between half and full step mode, between slow or fast decay
   * @param {any} decay 
   * @param {any} steps 
   * @memberof stepper
   */
  setMode(decay, steps) {
    if ((decay !== SLOW_DECAY) && (decay !== FAST_DECAY));
  }
}

module.exports = stepper;
